"use strict";

/**
 * @file Exemple de comment utiliser les librairies DOM.js & Node.js
 * @author Maxime Moreau
 */

console.clear(); // Nettoie la console

// -------------------------------------------------------------------------------

var fruit = Node.id("fruit");

// -------------------------------------------------------------------------------

/* .:: Affiche dans la console l'index d'un noeud via le noeud parent  ::. */
//DOM.eventIndex(fruit);

// -------------------------------------------------------------------------------

/* .:: Insére un noeud au début du noeud parent  ::. */
//DOM.insertFirst(fruit, Node.new("li", "Ananas"));

// -------------------------------------------------------------------------------

/* .:: Insére un noeud à la fin du noeud parent  ::. */
//DOM.insertLast(fruit, Node.new("li", "Cerise"));

// -------------------------------------------------------------------------------

/* .:: Insére un nouveau noeud à l'index choisi dans le noeud parent ::. */
DOM.insertIn(Node.tag("ul")[1], Node.setText(Node.addAttr(Node.new("li"), "class", "white bgBlue"), "Référencement Google"), 2);

// -------------------------------------------------------------------------------

/* .:: change les noeuds ::. */
DOM.NodeSwap(Node.id("fruit"), Node.id("book"));

// -------------------------------------------------------------------------------

/* .:: Supprime le dernier noeud du noeud parent ::. */
//DOM.removeLast(Node.id("fruit"));


// -------------------------------------------------------------------------------

/* .:: Supprime un noeud ::. */
//DOM.removeNode(Node.id("fruit"));

// -------------------------------------------------------------------------------

/* .:: Ajoute l'attribut dans le noeud ::. */
//Node.addAttr(fruit.children[4], "class", "red");
//Node.addAttr(fruit.children[4], "style", "font-size:100px;background-color:green;");

// -------------------------------------------------------------------------------

/* .:: Récupére le texte d'une noeud (Balise HTML) puis modifie le texte ::. */

/*
console.log(Node.getText(Node.id("fruit").children[0]));
Node.setText(Node.id("fruit").children[0], "Papaye");
console.log(Node.getText(Node.id("fruit").children[0]));
console.log(Node.getText(Node.id("divTest")));
Node.setText(Node.id("divTest"), "Hello");
*/

// -------------------------------------------------------------------------------

/* .:: Vérifie si un text d'un noeud existe ou non  ::. */

//console.log(Node.existText(Node.id("divTest")));

// -------------------------------------------------------------------------------

/* .:: Supprime l'attribut dans un noeud ::. */
//Node.removeAttr(Node.id("fruit"), "id");

// -------------------------------------------------------------------------------

/* .:: Récupére la valeur de l'attribut dans un noeud  ::. */
//console.log(Node.getAttrVal(Node.id("fruit"), "class"));

// -------------------------------------------------------------------------------

/* .:: Vérifie si un attribut est dans un noeud ::. */
//console.log(Node.hasAttr(Node.id("fruit"), "data-store"));
//console.log(Node.hasAttr(fruit, "data-store"));

// -------------------------------------------------------------------------------

/*
    .:: Créer un ensemble de noeud puis ajoute le noeud principal (div) à un noeud parent (après la liste des fruits) ::.

    Voici un ensemble de ce que l'on veut créer

    <div>
        <img src="https://www. ... .com/picture.png">
        <a href="https://www. ... .com/">Wikipedia C++</a>
    </div>
*/

var div = Node.new("div");
var picture = Node.addAttr(Node.new("img"), "src", "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/220px-ISO_C%2B%2B_Logo.svg.png");
var link = Node.setText(Node.addAttr(Node.new("a"), "href", "https://en.wikipedia.org/wiki/C%2B%2B"), "Wikipedia C++");
DOM.push(div, picture);
DOM.push(div, link);
DOM.insertNext(Node.id("fruit"), div);

// -------------------------------------------------------------------------------

/* .:: Modifier la valeur d'un attribut ::. */
//Node.setAttrVal(Node.id("fruit"), "data-store", "false demo123");

// -------------------------------------------------------------------------------

/* .:: Le noeud est vide (a t-il des balises HTML dans ce noeud) ? ::. */
//console.log(DOM.hasChild(Node.id("divTest")));

// -------------------------------------------------------------------------------