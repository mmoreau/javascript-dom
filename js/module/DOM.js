"use strict";

/**
 * @file Manipule les objets / méthodes / propriétes du DOM
 * @author Maxime Moreau
 * @version 0.1.0
 * @copyright Maxime Moreau - 2020
 */

class DOM 
{
	/*
	static eventIndex(parent)
	{
		parent.addEventListener("click", function(e)
		{
			if (parent === e.target.parentNode)
			{
				//console.log(e.target.innerHTML + " # " + index(e.target.parentNode.children, e.target));
				console.log("INDEX : " + this.index(e.target.parentNode.children, e.target) + " # " + e.target.textContent);
			}
		}, false);
	}*/



	/**
	 * Le noeud enfant sera insérer en premier du noeud parent
	 * 
	 * @param  {object} node_parent - Le noeud parent
	 * @param  {object} node_child - Le noeud enfant
	 * 
	 * @returns {object} - Les propriétés / methodes du noeud parent
	 * 
	 * @example
	 * 
	 * DOM.insertFirst(Node.id("fruit"), Node.new("li", "Ananas"));
	 * 
	 * Voir le fichier script.js
	 */
	static insertFirst(node_parent, node_child)
	{
		node_parent.insertBefore(node_child, node_parent.firstChild);

		return node_parent;
	}


	
	/**
	 * Le noeud enfant sera inséré à l'indice précisé dans le noeud parent
	 * 
	 * @param  {object} node_parent - Le noeud parent
	 * @param  {object} node_child - Le noeud enfant
	 * @param  {int} index=0 - L'indice / par défaut 
	 * 
	 * @returns {object} - Les propriétés / methodes du noeud parent
	 * 
	 * @example
	 * 
	 * DOM.insertIn(Node.id("book"), Node.setText(Node.addAttr(Node.new("li"), "class", "white bgBlue"), "Référencement Google"), 2);
	 * DOM.insertIn(Node.id("book"), Node.setText(Node.new("li"), "Introduction à la cybersécurité"), 2);
	 * 
	 * Voir le fichier script.js
	 */
	static insertIn(node_parent, node_child, index=0)
	{
		index = (index < 0) ? 0 : index;
		node_parent.insertBefore(node_child, node_parent.children[index]);

		return node_parent;
	}



	/**
	 * Le noeud enfant sera inséré en dernier du noeud parent
	 * 
	 * @param  {object} node_parent - Le noeud parent
	 * @param  {object} node_child - Le noeud enfant
	 * 
	 * @returns {object} - Les propriétés / methodes du noeud parent
	 * 
	 * @example
	 * 
	 * DOM.insertLast(Node.id("fruit"), Node.new("li", "Ananas"));
	 * 
	 * Voir le fichier script.js
	 */
	static insertLast(node_parent, node_child)
	{
		node_parent.insertBefore(node_child, node_parent.lastChild);

		return node_parent;
	}



	/**
	 * Le noeud enfant sera inséré avant le noeud parent
	 * 
	 * @param  {object} node_parent - Le noeud parent
	 * @param  {object} node_child - Le noeud enfant
	 * 
	 * @returns {object} - Les propriétés / methodes du noeud parent
	 * 
	 * @example
	 * 
	 * DOM.insertPrev(Node.id("fruit"), Node.setText(Node.new("div"), "Je suis inséré avant la liste des fruits"));
	 * 
	 * Voir le fichier script.js
	 */
	static insertPrev(node_parent, node_child)
	{
		node_parent.parentNode.insertBefore(node_child, node_parent.previousSibling);

		return node_parent;
	}



	/**
	 * Le noeud enfant sera inséré après le noeud parent
	 * 
	 * @param  {object} node_parent - Le noeud parent
	 * @param  {object} node_child - Le noeud enfant
	 * 
	 * @returns {object} - Les propriétés / methodes du noeud parent
	 * 
	 * @exemple
	 * 
	 * DOM.insertNext(Node.id("fruit"), Node.setText(Node.new("div"), "Je suis inséré après la liste des fruits"));
	 * 
	 * Voir le fichier script.js
	 */
	static insertNext(node_parent, node_child)
	{
		node_parent.parentNode.insertBefore(node_child, node_parent.nextSibling);

		return node_parent;
	}

	

	/**
	 * Supprime le premier noeud enfant dans un noeud parent si le noeud parent contient au moins un noeud enfant
	 * 
	 * @param  {object} node_parent - Le noeud parent
	 * 
	 * @returns {object} - Les propriétés / methodes du noeud parent
	 * 
	 * @example
	 * 
	 * DOM.removeFirst(Node.id("fruit"));
	 * DOM.removeFirst(Node.tag("ul")[0]);
	 * 
	 * Voir le fichier script.js
	 */
	static removeFirst(node_parent)
	{
    	if (node_parent.hasChildNodes())
    	{
        	node_parent.removeChild(node_parent.firstChild);
		}
		
		return node_parent;
	}



	/**
	 * Supprime un noeud
	 * 
	 * @param  {object} node - Le noeud
	 * 
	 * @returns {object} - Les propriétés / methodes du noeud
	 * 
	 * @exemple
	 * 
	 * DOM.removeNode(Node.id("fruit"));
	 * 
	 * Voir le fichier script.js
	 */
	static removeNode(node)
    {
		node.parentNode.removeChild(node);
		
		return node;
	}


	
	/**
	 * Supprime le dernier noeud enfant dans un noeud parent si le noeud parent contient au moins un noeud enfant
	 * 
	 * @param  {object} node_parent - Le noeud parent
	 * 
	 * @returns {object} - Les propriétés / methodes du noeud
	 * 
	 * @example
	 * 
	 * DOM.removeLast(Node.id("fruit"));
	 * DOM.removeLast(Node.tag("ul")[0]);
	 * 
	 * Voir le fichier script.js
	 */
	static removeLast(node_parent)
	{
    	if (node_parent.hasChildNodes())
    	{
        	if (node_parent.lastChild.nodeName == "#text")
        	{
            	node_parent.removeChild(node_parent.lastChild.previousSibling);
        	}
		}
		
		return node_parent;
	}


	
	/**
	 * Trouve le numéro d'index du noeud enfant dans le noeud parent
	 * 
	 * @param  {object} node_parent - Le noeud parent
	 * @param  {object} node_child - Le noeud enfant
	 * 
	 * @returns {int} - Le numéro d'index
	 */
	static index(node_parent, node_child)
	{
		return Array.prototype.indexOf.call(node_parent, node_child);
	}

    
	
	/*
	static display_child(parent)
	{
		for (let node of parent.children)
		{
			console.log(node.textContent);
		}
	}
	*/


	
	/**
	 * Échange les deux noeuds 
	 * 
	 * @param  {object} node - Le noeud 
	 * @param  {object} node2 - Le deuxième noeud
	 * 
	 * @example
	 * 
	 * DOM.NodeSwap(Node.id("fruit"), Node.id("book"));
	 * 
	 * Voir le fichier script.js
	 */
	static NodeSwap(node, node2)
	{
        // (1) - Clones the nodes so you don't lose data.
	    let clonedNode = node.cloneNode(true);
	    let clonedNode2 = node2.cloneNode(true);

        // (2) - Swap Node.
		node2.parentNode.replaceChild(clonedNode, node2);
		node.parentNode.replaceChild(clonedNode2, node);

        // (3) - Delete cloned data from nodes.
        clonedNode = null;
        clonedNode2 = null;
	}



	/**
	 * Ajoute à la fin un noeud dans un noeud parent 
	 * 
	 * @param  {object} node_parent - Le noeud parent
	 * @param  {object} node - Un noeud
	 * 
	 * @returns {object} - Les propriétés / methodes du noeud parent
	 * 
	 * @example 
	 * 
	 * var div = Node.new("div");
	   var picture = Node.addAttr(Node.new("img"), "src", "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/220px-ISO_C%2B%2B_Logo.svg.png");
	   var link = Node.setText(Node.addAttr(Node.new("a"), "href", "https://en.wikipedia.org/wiki/C%2B%2B"), "Wikipedia C++");
	   DOM.push(div, picture); // Ajoute le noeud picture dans la div
	   DOM.push(div, link);  // Ajoute le noeud link dans la div
	   DOM.insertNext(Node.id("fruit"), div);
	 * 
	 * Voir le fichier script.js
	 */
	static push(node_parent, node)
	{
		node_parent.appendChild(node);

		return node_parent;
	}



	/**
	 * Retourne si un noeud posséde au moins un noeud enfant
	 * 
	 * @param  {object} node - Le noeud
	 * 
	 * @returns {boolean} - True -> Posséde au moins un noeud enfant | False -> Ne posséde aucun noeud enfant
	 * 
	 * @example
	 * 
	 * console.log(DOM.hasChild(Node.id("divTest")));
	 * console.log(DOM.hasChild(Node.id("fruit")));
	 * 
	 * Voir le fichier script.js
	 */
	static hasChild(node)
	{
		//return node.childElementCount == 0;
		
		return !node.hasChildNodes();
	}
}