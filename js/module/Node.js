"use strict";

/**
 * @file Manipule les objets / méthodes / propriétes du noeud
 * @author Maxime Moreau
 * @version 0.1.0
 * @copyright Maxime Moreau - 2020
 */

class Node 
{	
	

	/**
	 * Retourne en objet toutes les propriétés / méthodes d'un noeud si seulement si le noeud a l'attribut "id" 
	 * 
	 * @param  {string} attribut_value - La valeur de l'attribut ID du noeud
	 * 
	 * @returns {object} - Les propriétés / méthodes du noeud
	 * 
	 * @example
	 * 
	 * Node.id("fruit");
	 * DOM.swap(Node.id("fruit"), Node.id("book"));
	 * DOM.insertLast(Node.id("fruit"), Node.new("li", "Cerise"));
	 * 
	 * Voir le fichier script.js
	 */
	static id(attribut_value)
	{
		return document.getElementById(attribut_value);
	}



	/**
	 * Retourne une collection d'objet de toutes les propriétés / méthodes des noeuds si seulement si les noeuds ont les valeurs indiqués dans l'attribut "class" 
	 * 
	 * @param  {string} attribut_values -  Les valeurs de l'attribut class des noeuds
	 * 
	 * @returns {object} - Les propriétés / méthodes des noeuds
	 * 
	 * @exemple
	 * 
	 * Node.class("demo")[0];
	 * DOM.swap(Node.class("demo")[0], Node.class("demo")[1]);
	 * 
	 * Voir le fichier script.js
	 */
	static class(attribut_values)
	{
		return document.getElementsByClassName(attribut_values);
	}


	
	/**
	 * Retourne une collection d'objet de toutes les propriétés / méthodes des noeuds qui ont pour le nom de la balise HTML indiqué
	 * 
	 * @param  {string} node_type - Le nom de la balise HTML
	 * 
	 * @returns {object} - Les propriétés / méthodes des noeuds
	 * 
	 * @example
	 * 
	 * Node.tag("ul")[0]
	 * DOM.swap(Node.tag("ul")[0], Node.tag("ul")[1]);
	 * 
	 * Voir le fichier script.js
	 */
	static tag(node_type)
	{
		return document.getElementsByTagName(node_type);
	}


	
	/**
	 * Retourne en objet toutes les propriétés / méthodes du noeud body
	 * 
	 * @returns {object} - Les propriétés / méthodes du noeud body
	 * 
	 * @example
	 * 
	 * console.log(Node.body());
	 */
	static body()
	{
		return document.body;
	}
	

    
	/**
	 * Créer un nouveau noeud sans l'ajouter au DOM
	 * 
	 * @param  {string} node_type - Le nom de la balise HTML
	 * 
	 * @returns {object} - Les propriétés / méthodes du nouveau noeud
	 * 
	 * @example
	 * 
	 * console.log(Node.new("li"));
	 * DOM.insertIn(Node.tag("ul")[1], Node.setText(Node.addAttr(Node.new("li"), "class", "white bgBlue"), "Référencement Google"), 2);
	 * DOM.insertLast(Node.id("fruit"), Node.new("li", "Cerise"));
	 * 
	 * Voir le fichier script.js
	 */
	static new(node_type)
	{
		return document.createElement(node_type);
	}


	
	/**
	 * Ajoute un attribut à un noeud 
	 * 
	 * @param  {object} node - L'objet du noeud
	 * @param  {string} attribute_name - Le nom de l'attribut
	 * @param  {string} attribute_value - La valeur de l'attribut
	 * @returns {object} - L'objet du noeud
	 * 
	 * @example
	 * Node.addAttr(Node.new("img"), "src", "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/220px-ISO_C%2B%2B_Logo.svg.png");
	 * Node.setText(Node.addAttr(Node.new("a"), "href", "https://en.wikipedia.org/wiki/C%2B%2B"), "Wikipedia C++");
	 * 
	 * Voir le fichier script.js
	 */
	static addAttr(node, attribute_name, attribute_value=null)
	{
		let createAttribute = document.createAttribute(attribute_name); 
		createAttribute.value = attribute_value;
		node.setAttributeNode(createAttribute);

		return node;
	}


	
	/**
	 * Vérifie si un noeud posséde un attribut
	 * 
	 * @param  {object} node - L'objet du noeud
	 * @param  {string} attribute_name - Le nom de l'attribut
	 * @returns {boolean} - True si l'attribut existe ou False si l'attribut n'existe pas
	 * 
	 * @example
	 * 
	 * console.log(Node.hasAttr(Node.id("fruit"), "data-store"));
	 * console.log(Node.hasAttr(Node.id("fruit"), "not-existing"));
	 * 
	 * Voir le fichier script.js
	 */
	static hasAttr(node, attribute_name)
	{
		//return node.attributes[attribute_name] != undefined;

		return node.hasAttribute(attribute_name);
	}


	
	/**
	 * Supprime un attribut du noeud
	 * 
	 * @param  {object} node - L'objet noeud
	 * @param  {string} attribute_name - Le nom de l'attribut
	 * @returns {object} - L'object du noeud
	 * 
	 * @example
	 * 
	 * Node.removeAttr(Node.id("fruit"), "id");
	 * Node.removeAttr(Node.id("fruit"), "data-store");
	 * 
	 * Voir le fichier script.js
	 */
	static removeAttr(node, attribute_name)
	{
		node.removeAttribute(attribute_name);

		return node;
	}



	/**
	 * Récupére la valeur de l'attribut du noeud
	 * 
	 * @param  {object} node - L'objet du noeud
	 * @param  {string} attribute_name - Le nom de l'attribut
	 * @returns {(string|undefined)} - La valeur de l'attribut
	 * 
	 * @example
	 * 
	 * console.log(Node.getAttrVal(Node.id("fruit", "class"))); 
	 * 
	 * Voir le fichier script.js
	 */
	static getAttrVal(node, attribute_name)
	{
		return this.inAttr(node, attribute_name) ? node.attributes[attribute_name].value : undefined;
	}



	/**
	 * Modifie la valeur de l'attribut du noeud
	 * 
	 * @param  {object} node - L'objet du noeud
	 * @param  {string} attribute_name - Le nom de l'attribut
	 * @param  {string} attribute_newvalue - La nouvelle valeur de l'attribut
	 * @param  {boolean} push=false - Concatène la nouvelle valeur de l'attribut à l'ancienne valeur de l'attribut si le paramètre est sur True sinon remplace la nouvelle valeur
	 * 
	 * @returns {object} - L'objet du noeud
	 * 
	 * @example
	 * 
	 * Node.setAttrVal(Node.id("fruit"), "data-store", "false demo123");
	 * 
	 * voir le fichier script.js
	 */
	static setAttrVal(node, attribute_name, attribute_newvalue, push=false)
	{
		if (this.inAttr(element, attribute_name))
		{		
			//node.attributes[attribute_name].value = push ? node.attributes[attribute_name].value.concat(attribute_newvalue) : attribute_newvalue;
			
			if (push)
			{
				node.attributes[attribute_name].value += attribute_newvalue;
			}
			else
			{
				node.attributes[attribute_name].value = attribute_newvalue;
			}
		}
		
		return node;
	}



	// Text
	/**
	 * Récupére le text d'un noeud
	 * 
	 * @param  {object} node - L'objet du noeud
	 * 
	 * @returns {string} - Le text
	 * 
	 * @example
	 * 
	 * console.log(Node.getText(Node.id("fruit").children[0]));
	 * console.log(Node.getText(Node.id("divTest"))); // <empty string>
	 * 
	 * Voir le fichier script.js
	 */
	static getText(node)
	{
		return node.textContent;
	}



	/**
	 * Modifie le text d'un noeud 
	 * 
	 * @param  {object} node - L'objet du noeud
	 * @param  {string} newText - Le nouveau text
	 * @param  {boolean} push=false - Concatène le nouveau text à l'ancien text si le paramètre est sur True sinon remplace le nouveau text
	 * 
	 * @returns {object} - L'objet du noeud
	 * 
	 * @example
	 * 
	 * Node.setText(Node.id("fruit").children[0], "Papaye");
	 * Node.setText(Node.id("divTest"), "Hello");
	 * 
	 * Voir le fichier script.js
	 */
	static setText(node, newText, push=false)
	{
		node.textContent = push ? node.textContent.concat(newText): newText;

		return node;
	}



	/**
	 * Vérifie si le text d'un noeud exist
	 * 
	 * @param  {object} node - L'objet du noeud
	 * 
	 * @returns {boolean} - True => existe | False => n'existe pas
	 * 
	 * @example
	 * 
	 * console.log(Node.existText(Node.id("divTest")));
	 * 
	 * Voir le fichier script.js
	 */
	static existText(node)
	{
		return node ? node.textContent != "" : false;
	}
}